import java.util.Scanner;

public class zaehlen {

	public static void main(String[] args) {
		aufgabe1b();
	}

	public static void aufgabe1a() {
		Scanner tastatur = new Scanner(System.in);

		System.out.println("Geben Sie eine Zahl ein: (Bis zu dieser Zahl wird vorgezahlt)");

		int bisZahl = tastatur.nextInt();
		int vonZahl = 1;

		while (vonZahl <= bisZahl) {
			System.out.print(vonZahl);
			if (vonZahl<bisZahl) {
				System.out.print(", ");
			}
			vonZahl++;
		}

	}

	public static void aufgabe1b() {
		Scanner tastatur = new Scanner(System.in);

		System.out.println("Geben Sie eine Zahl ein: (Von dieser Zahl wird heruntergez�hlt)");

		int Zahl3 = tastatur.nextInt();
		int Zahl4 = 1;

		while (Zahl3 >= Zahl4) {
			System.out.print(Zahl3);
			if (Zahl3>Zahl4) {
				System.out.print(", ");
			}
			Zahl3--;
		}
	}
}
