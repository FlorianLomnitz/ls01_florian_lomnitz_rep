import java.util.Scanner;

public class Zinseszins {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		boolean eingaben = false;
		double eingabeGeld, eingabeJahrZins;
		int eingabeJahre;
		String ausgabe;
		double result = 0, zinsen;

		System.out.print("Geldbetrag in Euro: ");
		eingabeGeld = s.nextDouble();
		System.out.print("Jahreszins (0,05 = 5%): ");
		eingabeJahrZins = s.nextDouble();
		System.out.print("Laufzeit in Jahren: ");
		eingabeJahre = s.nextInt();

		for (int i = 1; i < eingabeJahre + 1; i++) {

			if (i == 1) {
				zinsen = eingabeGeld * eingabeJahrZins;
				result = Math.round((eingabeGeld + zinsen) * 100.) / 100.;
				ausgabe = "Wert nach " + i + " Jahr: " + result + " EURO";
				System.out.println(ausgabe);
			} else {
				zinsen = result * eingabeJahrZins;
				result = Math.round((result + zinsen) * 100.) / 100.;
				ausgabe = "Wert nach " + i + " Jahren: " + result + " EURO";
				System.out.println(ausgabe);
			}
		}
	}

}