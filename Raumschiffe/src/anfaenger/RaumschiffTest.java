package anfaenger;

public class RaumschiffTest {

	public static void main(String[] args) {
		Raumschiff z1 = new Raumschiff();
//Energieversorgung
		z1.setBezeichnung("Energieversorgung");
		z1.setZustand(100);

		System.out.println(z1.getBezeichnung());
		System.out.println(z1.getZustand());

		Raumschiff z2 = new Raumschiff();
//Schild
		z2.setBezeichnung("Schild");
		z2.setZustand(100);

		System.out.println(z2.getBezeichnung());
		System.out.println(z2.getZustand());

		Raumschiff z3 = new Raumschiff();
//H�lle
		z3.setBezeichnung("H�lle");
		z3.setZustand(100);

		System.out.println(z3.getBezeichnung());
		System.out.println(z3.getZustand());

		Raumschiff z4 = new Raumschiff();
//Lebenserhaltungssystem
		z4.setBezeichnung("Lebenserhaltungssystem");
		z4.setZustand(100);

		System.out.println(z4.getBezeichnung());
		System.out.println(z4.getZustand());

//Ladung Photontorpedos
		Ladung l1 = new Ladung();

		l1.setBezeichnung("Photontorpedo");
		l1.setMenge(10);

		System.out.println(l1.getBezeichnung());
		System.out.println(l1.getMenge());

//Ladung Droiden		
		Ladung l2 = new Ladung();
		l2.setBezeichnung("Droiden");
		l2.setMenge(10);

		System.out.println(l2.getBezeichnung());
		System.out.println(l2.getMenge());

	}

}
