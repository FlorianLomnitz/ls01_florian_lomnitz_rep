package anfaenger;

public class Raumschiff {

	private String bezeichnung;
	private int zustand;

	public Raumschiff() {
		this.bezeichnung = "unknown";
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getZustand() {
		if (zustand > 0);
		return zustand;
	}

	public void setZustand(int zustand) {
		this.zustand = zustand;
	}
}
