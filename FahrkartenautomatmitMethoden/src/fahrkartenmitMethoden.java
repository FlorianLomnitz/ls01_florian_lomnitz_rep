import java.util.Scanner;

class Fahrkartenautomat_Methoden {
    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);

        int anzahlTickets;
        double zuZahlenderBetrag;
        double ticketPreis;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        double rueckgabebetrag;
        double eingegebenerBetrag;

        do
        {
        	System.out.println("Ticket (EURO-Cent):");
        	ticketPreis = tastatur.nextDouble();
        	
        }while(ticketPreis < 0);
        
        
        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();
        if(anzahlTickets >10 | anzahlTickets<0) 
        {
        anzahlTickets = 1;
        System.out.println("Ihre gew�hlte anzahl enspricht nicht dem zul�ssigen bereich");
        }
        zuZahlenderBetrag = ticketPreis * anzahlTickets;
        		
        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        
        System.out.println("\n\n");

        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(rueckgabebetrag);
    }

    		public static double fahrkartenbestellungErfassen() 
    		
    		{
    		Scanner tastatur = new Scanner(System.in);
    		int anzahlTickets;
    		double ticketPreis;
    		System.out.print("Ticketpreis (EURO-Cent): ");
    		ticketPreis = tastatur.nextDouble();
            System.out.print("Anzahl der Tickets: ");
            anzahlTickets = tastatur.nextInt();
            return ticketPreis * anzahlTickets;
            }
    		
    		public static double fahrkartenBezahlen(double zuZahlenderBetrag)
    		
    		{ Scanner tastatur = new Scanner(System.in);
    		double eingezahlterGesamtbetrag = 0.0;
    		
    		
            while (eingezahlterGesamtbetrag < zuZahlenderBetrag) 
            {
                System.out.format("Noch zu zahlen: %4.2f �%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
                System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
                double eingeworfeneMuenze = tastatur.nextDouble();
                eingezahlterGesamtbetrag += eingeworfeneMuenze;}
                
            
            
            return eingezahlterGesamtbetrag - zuZahlenderBetrag;}
    		
    		public static void fahrkartenAusgeben()
    		{System.out.println("\nFahrschein wird ausgegeben");
    		
            for (int i = 0; i < 8; i++) {
                System.out.print("=");
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
    		}
    		
    		public static double rueckgeldAusgeben(double rueckgabebetrag)
    		
    		{if (rueckgabebetrag > 0.0) {
                //***** L�sung der Ausgabenformatierungsaufgabe *****************/
                System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
                System.out.println("wird in folgenden M�nzen ausgezahlt:");

                while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
                {
                    System.out.println("2 EURO");
                    rueckgabebetrag -= 2.0;
                }
                while (rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
                {
                    System.out.println("1 EURO");
                    rueckgabebetrag -= 1.0;
                }
                while (rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
                {
                    System.out.println("50 CENT");
                    rueckgabebetrag -= 0.5;
                }
                while (rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
                {
                    System.out.println("20 CENT");
                    rueckgabebetrag -= 0.2;
                }
                while (rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
                {
                    System.out.println("10 CENT");
                    rueckgabebetrag -= 0.1;
                }
                while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
                {
                    System.out.println("5 CENT");
                    rueckgabebetrag -= 0.05;
                }
            }

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                    + "Wir w�nschen Ihnen eine gute Fahrt."); 
            return rueckgabebetrag;
    		}
    		
    		
    		
    		
    		
}