
public class Mittelwert {
	

	
	
   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      //m = (x + y) / 2.0;
      
      m= berechnetMittelwert(x,y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      printmittelwert(x,y,m);
   }
   
   public static void printmittelwert(double zahl1, double zahl2 , double erg) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl1, zahl2, erg);
   }
   
   
   public static double berechnetMittelwert(double zahl1, double zahl2) {
	   double erg= (zahl1 + zahl2) / 2.0;
	   return erg;
   }
}