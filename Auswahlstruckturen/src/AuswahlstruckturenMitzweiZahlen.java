import java.util.Scanner;

public class AuswahlstruckturenMitzweiZahlen {

	private static Scanner myScanner;

	public static void main(String[] args) {

		myScanner = new Scanner(System.in);

		// Geben Sie bitte die 1. Positive Zahl ein:

		System.out.println("Geben Sie bitte die 1. Zahl ein ");
		int zahl1 = myScanner.nextInt();

		// Geben Sie bitte die 2. Positive Zahl ein:

		System.out.println("Geben Sie die 2. Zahl ein ");
		int zahl2 = myScanner.nextInt();

		// Die Kleinere Zahl lautet

		aufgabe1a1(zahl1, zahl2);

		aufgabe1a2(zahl1, zahl2);
		
		aufgabe1a3(zahl1, zahl2);
	}

	public static void aufgabe1a1(int z1, int z2) {
		if (z1 > z2) {
			System.out.println("Die Gr��ere Zahl lautet " + z1);
		} else {
			System.out.println("Die Gr��ere Zahl lautet " + z2);
		}
	}

	public static void aufgabe1a2(int z1, int z2) {
		if (z1 < z2) {
			System.out.println("Die kleinere Zahl lautet " + z1);
		} else {
			System.out.println("Die kleinere Zahl lautet " + z2);

		}

	}

	public static void aufgabe1a3(int z1, int z2) {
		if (z1 == z2) {

			System.out.println("Die  Zahlen sind gleich Gro�  ");

		} else {
			System.out.println("Die Zahlen " + z1 + " und " + z2 +  " sind nicht gleich Gro� ");

		}

	}
}
